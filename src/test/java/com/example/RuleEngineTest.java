package com.example;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RuleEngineTest {
    RuleEngine ruleEngine = new RuleEngine();

    @Test
    public void shouldReturnCorrectScoreIfBothMovesAreCooperate() {
        int[] expectedScores = {2, 2};
        Scores scores = ruleEngine.calculateScore(Moves.COOPERATE, Moves.COOPERATE);
        assertEquals(expectedScores[0], scores.getPlayer1Score());
        assertEquals(expectedScores[1], scores.getPlayer2Score());
    }

    @Test
    public void shouldReturnCorrectScoreIfBothMovesAreCheat() {
        int[] expectedScores = {0, 0};
        Scores scores = ruleEngine.calculateScore(Moves.CHEAT, Moves.CHEAT);
        assertEquals(expectedScores[0], scores.getPlayer1Score());
        assertEquals(expectedScores[1], scores.getPlayer2Score());
    }

    @Test
    public void shouldReturnCorrectScoreIfOneMovesIsCheatAndAnotherIsCooperate() {
        int[] expectedScores = {3, -1};
        Scores scores = ruleEngine.calculateScore(Moves.CHEAT, Moves.COOPERATE);
        assertEquals(expectedScores[0], scores.getPlayer1Score());
        assertEquals(expectedScores[1], scores.getPlayer2Score());
    }

    @Test
    public void shouldReturnCorrectScoreIfOneMovesIsCooperateAndAnotherIsCheat() {
        int[] expectedScores = {-1, 3};
        Scores scores = ruleEngine.calculateScore(Moves.COOPERATE, Moves.CHEAT);
        assertEquals(expectedScores[0], scores.getPlayer1Score());
        assertEquals(expectedScores[1], scores.getPlayer2Score());
    }
}
