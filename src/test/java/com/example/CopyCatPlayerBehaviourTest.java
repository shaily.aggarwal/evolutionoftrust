package com.example;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CopyCatPlayerBehaviourTest {

    @Test
    public void shouldCooperateInFirstMove() {
        CopyCatPlayerBehaviour player = new CopyCatPlayerBehaviour();
        assertEquals(Moves.COOPERATE, player.getMove());
    }

    @Test
    public void shouldCopyLastMoveOfOpponent() {
        CopyCatPlayerBehaviour player = new CopyCatPlayerBehaviour();
        player.informOpponentsMove(Moves.CHEAT);
        assertEquals(Moves.CHEAT, player.getMove());
    }
}
