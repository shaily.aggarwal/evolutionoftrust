package com.example;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DetectivePlayerTest {

    @Test
    public void shouldMakeFirstFourFixedMoves() {
        DetectivePlayerBehaviour detectivePlayer = new DetectivePlayerBehaviour();

        assertEquals(Moves.COOPERATE, detectivePlayer.getMove());
        assertEquals(Moves.CHEAT, detectivePlayer.getMove());
        assertEquals(Moves.COOPERATE, detectivePlayer.getMove());
        assertEquals(Moves.COOPERATE, detectivePlayer.getMove());
    }
}
