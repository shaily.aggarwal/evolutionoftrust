package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {
    private Game game;

    @Mock
    ScannerWrapper mockedScannerWrapper;

    @Test
    public void shouldUpdateCorrectScoreIfBothPlayerAlwaysCooperate() {
        Player p1 = new Player(new CooperativePlayerBehaviour());
        Player p2 = new Player(new CooperativePlayerBehaviour());
        game = new Game(p1, p2);
        game.play(1);
        assertEquals(2, p1.getScore(), 0);
        assertEquals(2, p2.getScore(), 0);
    }

    @Test
    public void shouldUpdateCorrectScoreIfBothPlayerAlwaysCheat() {
        Player p1 = new Player(new CheatPlayerBehaviour());
        Player p2 = new Player(new CheatPlayerBehaviour());
        game = new Game(p1, p2);
        game.play(1);
        assertEquals(0, p1.getScore(), 0);
        assertEquals(0, p2.getScore(), 0);
    }

    @Test
    public void shouldUpdateCorrectScoreIfOnePlayerAlwaysCheatOneAlwaysCooperates() {
        Player p1 = new Player(new CheatPlayerBehaviour());
        Player p2 = new Player(new CooperativePlayerBehaviour());
        game = new Game(p1, p2);
        game.play(1);
        assertEquals(3, p1.getScore(), 0);
        assertEquals(-1, p2.getScore(), 0);
    }

    @Test
    public void shouldUpdateCorrectScoreIfOnePlayerAlwaysCooperateOneAlwaysCheatsForMultipleRounds() {
        Player p1 = new Player(new CooperativePlayerBehaviour());
        Player p2 = new Player(new CheatPlayerBehaviour());
        game = new Game(p1, p2);
        game.play(2);
        assertEquals(-2, p1.getScore(), 0);
        assertEquals(6, p2.getScore(), 0);
    }

    @Test
    public void shouldUpdateCorrectScoreIfOnePlayerAlwaysCooperateAndAnotherIsConsole() {
        Player p1 = new Player(new CooperativePlayerBehaviour());
        Player p2 = new Player(new ConsolePlayerBehaviour(mockedScannerWrapper));
        when(mockedScannerWrapper.readInput()).thenReturn("CHEAT");

        game = new Game(p1, p2);
        game.play(1);
        assertEquals(-1, p1.getScore(), 0);
        assertEquals(3, p2.getScore(), 0);
    }

    @Test
    public void shouldUpdateCorrectScoreIfOnePlayerAlwaysCheatAndAnotherIsCopyCat() {
        Player p1 = new Player(new CheatPlayerBehaviour());
        Player p2 = new Player(new CopyCatPlayerBehaviour());

        game = new Game(p1, p2);
        game.play(2);
        assertEquals(3, p1.getScore(), 0);
        assertEquals(-1, p2.getScore(), 0);
    }

    @Test
    public void shouldUpdateCorrectScoreIfOnePlayerIsCopyCatAndAnotherAlwaysCheat() {
        Player p1 = new Player(new CopyCatPlayerBehaviour());
        Player p2 = new Player(new CheatPlayerBehaviour());

        game = new Game(p1, p2);
        game.play(2);
        assertEquals(-1, p1.getScore(), 0);
        assertEquals(3, p2.getScore(), 0);
    }

    @Test
    public void shouldUpdateCorrectScoreIfBothAresCopyCat() {
        Player p1 = new Player(new CopyCatPlayerBehaviour());
        Player p2 = new Player(new CopyCatPlayerBehaviour());

        game = new Game(p1, p2);
        game.play(2);
        assertEquals(4, p1.getScore(), 0);
        assertEquals(4, p2.getScore(), 0);
    }

    @Test
    public void shouldUpdateCorrectScoreIfOnePlayerIsCopyCatAndAnotherIsConsole() {
        Player p1 = new Player(new CopyCatPlayerBehaviour());
        Player p2 = new Player(new ConsolePlayerBehaviour(mockedScannerWrapper));
        when(mockedScannerWrapper.readInput()).thenReturn("CHEAT","COOPERATE", "CHEAT", "CHEAT");

        game = new Game(p1, p2);
        game.play(4);
        assertEquals(1, p1.getScore(), 0);
        assertEquals(5, p2.getScore(), 0);
    }

    @Test
    public void shouldUpdateCorrectScoreIfOnePlayerIsDetectiveAndAnotherIsCooperate() {
        Player p1 = new Player(new DetectivePlayerBehaviour());
        Player p2 = new Player(new CooperativePlayerBehaviour());

        game = new Game(p1, p2);
        game.play(5);
        assertEquals(12, p1.getScore(), 0);
        assertEquals(4, p2.getScore(), 0);
    }

    @Test
    public void shouldUpdateCorrectScoreIfOnePlayerIsDetectiveAndAnotherIsConsole() {
        Player p1 = new Player(new DetectivePlayerBehaviour());
        Player p2 = new Player(new ConsolePlayerBehaviour(mockedScannerWrapper));
        when(mockedScannerWrapper.readInput()).thenReturn("COOPERATE","COOPERATE", "COOPERATE", "COOPERATE", "COOPERATE"
                , "CHEAT", "COOPERATE");

        game = new Game(p1, p2);
        game.play(7);
        assertEquals(15, p1.getScore(), 0);
        assertEquals(3, p2.getScore(), 0);
    }
}
