package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConsolePlayerBehaviourTest {

    @Mock
    ScannerWrapper mockedScannerWrapper;

    @Test
    public void shouldReturnCorrectMoveInputByUser() {
        when(mockedScannerWrapper.readInput()).thenReturn("CHEAT");
        ConsolePlayerBehaviour player = new ConsolePlayerBehaviour(mockedScannerWrapper);
        assertEquals(Moves.CHEAT, player.getMove());
    }
}
