package com.example;

import java.util.Scanner;

public class ScannerWrapper {
    private Scanner scanner = new Scanner(System.in);

    public String readInput() {
        return scanner.next();
    }
}
