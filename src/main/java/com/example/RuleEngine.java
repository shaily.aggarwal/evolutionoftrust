package com.example;

public class RuleEngine {
    public Scores calculateScore(Moves player1Move, Moves player2Move) {
        if (player1Move == Moves.COOPERATE && player2Move == Moves.CHEAT)
            return new Scores(-1, 3);
        else if (player1Move == Moves.CHEAT && player2Move == Moves.COOPERATE)
            return new Scores(3, -1);
        else if (player1Move == Moves.COOPERATE)
            return new Scores(2, 2);
        else
            return new Scores(0, 0);
    }
}