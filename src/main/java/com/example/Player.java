package com.example;

import java.util.Observable;
import java.util.Observer;

public class Player implements Observer {
    private int score;
    private Moves move;
    private PlayerBehaviour playerBehaviour;

    public void setMoveAndNotifyObservers() {
        this.move = playerBehaviour.getMove();

    }

    public Player(PlayerBehaviour playerBehaviour) {
        this.playerBehaviour = playerBehaviour;
    }

    void setScore(int score) {
        this.score += score;
    }

    public Moves getMove() {
        setMoveAndNotifyObservers();
        return this.move;
    }

    public int getScore() {
        return score;
    }

    @Override
    public void update(Observable o, Object move) {
        playerBehaviour.informOpponentsMove((Moves) move);
    }
}
