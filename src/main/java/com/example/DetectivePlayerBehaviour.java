package com.example;

public class DetectivePlayerBehaviour extends CopyCatPlayerBehaviour {
    private final Moves[] firstFourMoves = {Moves.COOPERATE, Moves.CHEAT, Moves.COOPERATE, Moves.COOPERATE};
    private boolean opponentCheated = false;
    private int rounds = -1;

    public Moves getMove() {
        rounds++;
        if (rounds < 4)
            return firstFourMoves[rounds];
        if (opponentCheated)
            return super.getMove();

        return Moves.CHEAT;
    }

    @Override
    public void informOpponentsMove(Moves opponentMove) {
        if (opponentCheated && opponentMove.equals(Moves.CHEAT))
            opponentCheated = false;

        super.informOpponentsMove(opponentMove);
    }
}
