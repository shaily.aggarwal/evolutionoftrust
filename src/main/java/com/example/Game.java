package com.example;

import java.util.Observable;

public class Game extends Observable {
    private Player p1;
    private Player p2;
    RuleEngine ruleEngine;

    public Game(Player p1, Player p2) {
        this.p1 = p1;
        this.p2 = p2;
        addObserver(p1);
        addObserver(p2);
    }

    private void updateScores() {
        Moves p1Move = p1.getMove();
        setChanged();
        notifyObservers(p1Move);

        Moves p2Move = p2.getMove();
        setChanged();
        notifyObservers(p2Move);
        Scores scores = ruleEngine.calculateScore(p1Move, p2Move);

        p1.setScore(scores.getPlayer1Score());
        p2.setScore(scores.getPlayer2Score());
    }

    public void play(int rounds) {
        ruleEngine = new RuleEngine();

        for (int i = 0; i < rounds; i++) {
            updateScores();
        }
    }
}
