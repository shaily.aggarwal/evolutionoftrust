package com.example;

public enum Moves {
    CHEAT("ch"), COOPERATE("co");
    private String value;

    Moves(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}