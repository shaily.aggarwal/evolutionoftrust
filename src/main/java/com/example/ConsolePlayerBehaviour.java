package com.example;

public class ConsolePlayerBehaviour extends PlayerBehaviour {
    private ScannerWrapper scannerWrapper;

    public ConsolePlayerBehaviour(ScannerWrapper scannerWrapper){
        super();
        this.scannerWrapper = scannerWrapper;
    }

    public Moves getMove() {
        String input = scannerWrapper.readInput();
        Moves move = Moves.valueOf(input);
        return move;
    }
}
