package com.example;

import java.util.Observable;
import java.util.Observer;

public class CopyCatPlayerBehaviour extends PlayerBehaviour {
    private Moves opponentMove;

    @Override
    public void informOpponentsMove(Moves opponentMove) {
        this.opponentMove = opponentMove;
    }

    public Moves getMove() {
        if (opponentMove == null)
            opponentMove = Moves.COOPERATE;
        return opponentMove;
    }
}
